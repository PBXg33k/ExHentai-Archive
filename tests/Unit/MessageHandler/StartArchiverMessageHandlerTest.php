<?php
namespace App\Tests\Unit\MessageHandler;

use App\Entity\ExhentaiGallery;
use App\Event\ExhentaiGalleryCreatedEvent;
use App\Message\StartArchiverMessage;
use App\MessageHandler\StartArchiverMessageHandler;
use App\Model\GalleryArchiveQueueItem;
use App\Repository\ExhentaiGalleryRepository;
use App\Service\ExHentaiBrowserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class StartArchiverMessageHandlerTest extends TestCase
{
    /**
     * @var MockObject|ContainerInterface
     */
    private $container;

    /**
     * @var MockObject|AdapterInterface
     */
    private $cache;

    /**
     * @var MockObject|CacheItemInterface
     */
    private $cacheItem;

    /**
     * @var MockObject|LoggerInterface
     */
    private $logger;

    /**
     * @var MockObject|ExhentaiGalleryRepository
     */
    private $galleryRepository;

    /**
     * @var MockObject|ExHentaiBrowserService
     */
    private $browserService;

    /**
     * @var MockObject|EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var StartArchiverMessageHandler
     */
    private $messageHandler;

    /**
     * @var StartArchiverMessage
     */
    private $startMessage;

    protected function setUp()
    {
        $this->container = $this->getMockBuilder(ContainerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->cache = $this->getMockBuilder(AdapterInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->cacheItem = $this->getMockBuilder(CacheItemInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->galleryRepository = $this->getMockBuilder(ExhentaiGalleryRepository::class)
            ->disableOriginalConstructor()->getMock();

        $this->browserService = $this->getMockBuilder(ExHentaiBrowserService::class)
            ->disableOriginalConstructor()->getMock();

        $this->eventDispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $doctrineMock = $this->getMockBuilder(ManagerRegistry::class)
            ->disableOriginalConstructor()->getMock();

        $doctrineMock->expects($this->once())
            ->method('getRepository')
            ->with(
                ExhentaiGallery::class
            )
            ->willReturn($this->galleryRepository);

        $this->container->expects($this->at(0))
            ->method('get')
            ->with('cache.app')
            ->willReturn($this->cache);

        $this->container->expects($this->at(1))
            ->method('get')
            ->with('doctrine')
            ->willReturn($doctrineMock);

        $this->startMessage = new StartArchiverMessage();

        $this->messageHandler = new StartArchiverMessageHandler(
            $this->container,
            $this->logger,
            $this->browserService,
            $this->eventDispatcher
        );
    }

    /**
     * @test
     */
    public function canRun()
    {
        // Optional: Test anything here, if you want.
        $this->assertTrue(true, 'This should already work.');

        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
        return;
        $handler = $this->messageHandler;

        $this->eventDispatcher->expects($this->once())
            ->method('addListener')
            ->with(
                $this->equalTo(ExhentaiGalleryCreatedEvent::NAME),
                $this->anything()
            );

        $this->cache->expects($this->exactly(2))
            ->method('getItem')
            ->withConsecutive(
                $this->equalTo(StartArchiverMessageHandler::CACHE_KEY),
                $this->equalTo(GalleryArchiveQueueItem::CACHE_KEY)
            )
            ->willReturn($this->cacheItem);

        $this->cacheItem->expects($this->at(0))
            ->method('isHit')
            ->willReturn(false);

//        $this->cacheItem->expects($this->at(0))
//            ->method('set')
//            ->with(
//                $this->isInstanceOf(\DateTimeInterface::class)
//            );

//        $this->cache->expects($this->at(0))
//            ->method('save')
//            ->with($this->cacheItem);


        $this->galleryRepository->expects($this->once())
            ->method('getByDownloadState')
            ->with(
                ExhentaiGallery::DOWNLOAD_STATE_QUEUED
            )
            ->willReturn(
                new ArrayCollection()
            );

        $this->cacheItem->expects($this->at(1))
            ->method('isHit')
            ->willReturn(true);

        $this->cacheItem->expects($this->once())
            ->method('get')
            ->willReturn(
                new ArrayCollection([
                    new GalleryArchiveQueueItem(1, 'a')
                ])
            );

        $galleryMock = new ExhentaiGallery();

        $this->browserService->expects($this->once())
            ->method('getGalleries')
            ->willReturn(new ArrayCollection([$galleryMock]));

        $this->browserService->expects($this->once())
            ->method('downloadGalleryZip')
            ->with($galleryMock);

        $handler($this->startMessage);
    }
}
