<?php
namespace App\Tests\Unit\Service;

use App\Service\ExHentaiBrowserService;
use App\Service\LegacyBridgeService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class LegacyBridgeServiceTest extends TestCase
{
    /**
     * @var MockObject|LoggerInterface
     */
    private $logger;

    /**
     * @var MockObject|ExHentaiBrowserService
     */
    private $browserService;

    /**
     * @var LegacyBridgeService
     */
    private $legacyBridgeService;

    protected function setUp()
    {
        $this->logger = $logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->browserService = $browserService = $this->getMockBuilder(ExHentaiBrowserService::class)
            ->disableOriginalConstructor()->getMock();

        $this->legacyBridgeService = new LegacyBridgeService($this->logger, $this->browserService);

        parent::setUp(); // TODO: Change the autogenerated stub
    }

    public function testGetLogger()
    {
        $this->assertSame($this->logger, $this->legacyBridgeService->getLogger());
    }

    public function testGetBrowser()
    {
        $this->assertSame($this->browserService, $this->legacyBridgeService->getBrowser());
    }
}
