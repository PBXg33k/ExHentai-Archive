<?php
namespace App\Tests\Unit\Event;

use App\Entity\ExhentaiGallery;
use App\Event\ExhentaiGalleryCreatedEvent;
use PHPUnit\Framework\TestCase;

class ExhentaiGalleryCreatedEventTest extends TestCase
{
    public function testGetGallery()
    {
        $gallery = (new ExhentaiGallery())->setId(1)->setToken('123abc');

        $event = new ExhentaiGalleryCreatedEvent($gallery);
        $this->assertSame($gallery, $event->getGallery());
    }
}
