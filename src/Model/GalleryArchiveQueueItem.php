<?php
namespace App\Model;

class GalleryArchiveQueueItem extends GalleryToken
{
    const CACHE_KEY = 'gallery_archive_queue';

    /**
     * @var \DateTime
     */
    private $timestamp;

    /**
     * GalleryArchiveQueueItem constructor.
     * @param int $galleryId
     * @param string $galleryToken
     */
    public function __construct(int $galleryId, string $galleryToken)
    {
        $this->timestamp    = new \DateTime();
        parent::__construct($galleryId, $galleryToken);
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }
}
