<?php
namespace App\Model;

use App\Entity\ExhentaiGallery;
use App\Exception\HentaiArchiveException;
use App\Exception\HentaiArchiveFileCountMismatchException;
use App\Exception\HentaiArchiveFilesizeMismatchException;
use App\Exception\HentaiException;

class GalleryArchive extends \ZipArchive
{
    /**
     * @var ExhentaiGallery
     */
    private $gallery;

    /**
     * @var \SplFileInfo
     */
    private $fileInfo;

    /**
     * @var int combined size of files in archive
     */
    public $contentSize    = 0;

    /**
     * @var int compressed size of files in archive
     */
    public $compressedSize = 0;

    public function __construct($zipFile, ExhentaiGallery $gallery = null)
    {
        if ($gallery) {
            $this->gallery = $gallery;
        }

        $res = false;

        if (is_string($zipFile) && is_file($zipFile) && is_readable($zipFile)) {
            $this->setFileInfo(new \SplFileInfo($zipFile));
        } elseif ($zipFile instanceof \SplFileInfo) {
            $this->setFileInfo($zipFile);
        }

        if ($this->fileInfo) {
            $res = $this->open($this->getPath());
        } else {
            throw new HentaiArchiveException($this, null, 'Invalid path passed: '. $zipFile);
        }

        if ($res !== true && $res !== 0) {
            throw new HentaiArchiveException($this, null, "Failed opening archive, code: {$res} - " . $this->returnErrorMessage($res));
        }

        $this->initializeFile();
    }

    /**
     * @return ExhentaiGallery
     */
    public function getGallery(): ExhentaiGallery
    {
        return $this->gallery;
    }

    /**
     * @param ExhentaiGallery $gallery
     * @return GalleryArchive
     */
    public function setGallery(ExhentaiGallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getSearchName()
    {
        return substr($this->fileInfo->getFilename(), 0, -4);
    }

    public function getPath()
    {
        return $this->fileInfo->getRealPath();
    }

    public function checkArchiveValidity()
    {
        if ($this->numFiles == $this->gallery->getFileCount()) {
            if ($this->contentSize == $this->gallery->getFilesize()) {
                    return true;
            }
            throw new HentaiArchiveFilesizeMismatchException($this, $this->gallery);
        }
        throw new HentaiArchiveFileCountMismatchException($this, $this->gallery);
    }

    private function initializeFile()
    {
        for ($i=0; $i<=$this->numFiles; $i++) {
            $stat = $this->statIndex($i);
            $this->contentSize += $stat['size'];
            $this->compressedSize += $stat['comp_size'];
        }
    }

    private function setFileInfo(\SplFileInfo $fileInfo)
    {
        $this->fileInfo = $fileInfo;
    }

    public function __destruct()
    {
        $this->close();
    }

    private function returnErrorMessage(int $errorCode)
    {
        // https://github.com/nih-at/libzip/blob/master/lib/zip.h#L105
        $map = [
            0  => 'No error',
            1  => 'Multi-disk zip archives not supported',
            2  => 'Renaming temporary file failed',
            3  => 'Closing zip archive failed',
            4  => 'Seek error',
            5  => 'Read error',
            6  => 'Write error',
            7  => 'CRC error',
            8  => 'Containing zip archive was closed',
            9  => 'No such file',
            10 => 'File already exists',
            11 => 'Can\'t open file',
            12 => 'Failure to create temporary file',
            13 => 'Zlib error',
            14 => 'Malloc failure',
            15 => 'Entry has been changed',
            16 => 'Compression method not supported',
            17 => 'Premature end of file',
            18 => 'Invalid argument',
            19 => 'Not a zip archive',
            20 => 'Internal error',
            21 => 'Zip archive inconsistent',
            22 => 'Can\'t remove file',
            23 => 'Entry has been deleted',
            24 => 'Encryption method not supported',
            25 => 'Read-only archive',
            26 => 'No password provided',
            27 => 'Wrong password provided',
            28 => 'Operation not supported',
            29 => 'Resource still in use',
            30 => 'Tell error',
            31 => 'Compressed data invalid',
        ];

        if (array_key_exists($errorCode, $map)) {
            return $map[$errorCode];
        } else {
            return false;
        }
    }
}
