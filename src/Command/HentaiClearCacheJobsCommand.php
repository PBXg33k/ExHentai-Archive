<?php

namespace App\Command;

use App\MessageHandler\StartArchiverMessageHandler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HentaiClearCacheJobsCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'hentai:clear-cache-jobs';

    /**
     * @var AdapterInterface
     */
    private $cache;

    protected function configure()
    {
        $this
            ->setDescription('Will clear running jobs from cache, in case job has crashed or cannot restart')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Clear all jobs')
            ->addOption('archiver', 'r', InputOption::VALUE_NONE, 'Clear archiver job')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cache = $this->getContainer()->get('cache.app');

        $io = new SymfonyStyle($input, $output);
        $all = $input->getOption('all');

        if ($all || $input->getOption('archiver')) {
            if ($this->cache->deleteItem(StartArchiverMessageHandler::CACHE_KEY)) {
                $io->success('Cleared archiver job');
            } else {
                $io->error('Failed clearing archiver job');
            }
        }

        $io->success('Done');
    }
}
