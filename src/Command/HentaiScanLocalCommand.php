<?php

namespace App\Command;

use App\Entity\ExhentaiGallery;
use App\Exception\HentaiArchiveException;
use App\Model\GalleryArchive;
use App\Service\ArchiveService;
use Elastica\Query\Match;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class HentaiScanLocalCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'hentai:scan-local';
    protected $zipArchive;

    /**
     * @var ArchiveService
     */
    protected $archiveService;

    public function __construct(?string $name = null)
    {
        $this->zipArchive = new \ZipArchive();
        parent::__construct($name);
    }

    protected function configure()
    {

        $this
            ->setDescription('Scans the given directory for Gallery zipfiles')
            ->addArgument('path', InputArgument::REQUIRED, 'Starting directory')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->archiveService = $this->getContainer()->get('hentai.archive');
        $elasticaFinder = $this->getContainer()->get('fos_elastica.finder.app.gallery');

        $io = new SymfonyStyle($input, $output);
        $directoryPath = $input->getArgument('path');

        $filesystem = new Filesystem();
        $finder = new Finder();
        $hit = $filemiss = $sizemiss = $countmiss = 0;

        if ($filesystem->exists($directoryPath)) {
            $inodes = [];
            $finder->files()->name('*.zip')->in($directoryPath)->filter(function (\SplFileInfo $file) use ($inodes) {
                if (!in_array($file->getInode(), $inodes)) {
                    $inodes[] = $file->getInode();
                    return true;
                }
                return false;
            });

            $io->note(sprintf('Found %d files', $finder->count()));

            foreach ($finder as $file) {
                $match = false;
                try {
                    $archive = new GalleryArchive($file);
                } catch (HentaiArchiveException $exception) {
                    $io->error(sprintf(
                        "Error opening archive %s",
                        $exception->getGalleryArchive()->filename
                    ));
                    continue;
                }

                // @todo put elastica logic in a service
                $fieldQuery = new Match();
                $fieldQuery->setFieldQuery('title', $archive->getSearchName());
                $pagination = $elasticaFinder->findPaginated($fieldQuery);
                $results = $pagination->getCurrentPageResults();
                /** @var ExhentaiGallery $result */
                foreach ($results as $result) {
                    $searchName = substr($file->getFilename(), 0, -4);
                    $filteredName = str_replace(['?','|', '\'','"','~'], ' ', $result->getTitle());

                    if ($searchName == $filteredName) {
                        $archive->setGallery($result);
                        try {
                            $this->archiveService->addArchiveToGallery($archive, $result);
                            $hit++;
                            $match = true;
                        } catch (HentaiArchiveException $exception) {
                        }
                    }
                }

                if (!$match) {
                    $filemiss++;
                }

                unset($results, $pagination, $fieldQuery);
            }

            $io->note(sprintf(
                "HIT: %d FILEMISS: %d COUNTMISS: %d SIZEMISS: %d",
                $hit,
                $filemiss,
                $countmiss,
                $sizemiss
            ));
            $io->success(sprintf('Finished scanning local files. Found %d files of which %d were matches', $finder->count(), $hit));
        } else {
            $io->error('Directory not found or readable');
        }
    }
}
