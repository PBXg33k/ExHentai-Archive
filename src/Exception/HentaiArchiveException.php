<?php
namespace App\Exception;

use App\Entity\ExhentaiGallery;
use App\Model\GalleryArchive;
use Throwable;

class HentaiArchiveException extends HentaiGalleryException
{
    /**
     * @var GalleryArchive
     */
    private $galleryArchive;

    public function __construct(GalleryArchive $galleryArchive, ExhentaiGallery $gallery = null, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->galleryArchive = $galleryArchive;
        parent::__construct($gallery, $message, $code, $previous);
    }

    /**
     * @return GalleryArchive
     */
    public function getGalleryArchive(): GalleryArchive
    {
        return $this->galleryArchive;
    }
}
