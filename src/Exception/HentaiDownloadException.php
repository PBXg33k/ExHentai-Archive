<?php
namespace App\Exception;

use App\Entity\ExhentaiGallery;
use Throwable;

class HentaiDownloadException extends HentaiGalleryException
{
    /**
     * @var string
     */
    protected $html;

    public function __construct(ExhentaiGallery $gallery = null, string $message = "", string $html = '', int $code = 0, Throwable $previous = null)
    {
        $this->html = $html;
        parent::__construct($gallery, $message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }
}
