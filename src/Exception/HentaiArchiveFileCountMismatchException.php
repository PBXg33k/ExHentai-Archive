<?php
namespace App\Exception;

use App\Entity\ExhentaiGallery;
use App\Model\GalleryArchive;
use Throwable;

class HentaiArchiveFileCountMismatchException extends HentaiArchiveException
{
    public function __construct(GalleryArchive $galleryArchive, ExhentaiGallery $gallery, string $message = "", int $code = 0, Throwable $previous = null)
    {
        if ($message == "") {
            $message = sprintf(
                "Images in archive and gallery does not match. Expected %d, got %d",
                $gallery->getFileCount(),
                $galleryArchive->numFiles
            );
        }
        parent::__construct($galleryArchive, $gallery, $message, $code, $previous);
    }
}
