<?php
namespace App\Exception;

use App\Entity\ExhentaiGallery;
use Throwable;

class HentaiGalleryException extends HentaiException
{
    /**
     * @var ExhentaiGallery
     */
    private $gallery;

    public function __construct(ExhentaiGallery $gallery = null, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->gallery = $gallery;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ExhentaiGallery
     */
    public function getGallery(): ExhentaiGallery
    {
        return $this->gallery;
    }
}
