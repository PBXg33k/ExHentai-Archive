<?php
namespace App\Exception;

use App\Entity\ExhentaiGallery;
use App\Model\GalleryArchive;
use Throwable;

class HentaiArchiveFilesizeMismatchException extends HentaiArchiveException
{
    public function __construct(GalleryArchive $galleryArchive, ExhentaiGallery $gallery, string $message = "", int $code = 0, Throwable $previous = null)
    {
        if ($message === "") {
            $message = sprintf(
                "Archive filesize mismatch. Expected %d, got %d",
                $gallery->getFilesize(),
                $galleryArchive->contentSize
            );
        }

        parent::__construct($galleryArchive, $gallery, $message, $code, $previous);
    }
}
