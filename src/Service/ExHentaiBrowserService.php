<?php
namespace App\Service;

use App\Entity\ExhentaiGallery;
use App\Event\ExhentaiGalleryCreatedEvent;
use App\Exception\HentaiDownloadException;
use App\Exception\HentaiException;
use App\Model\GalleryToken;
use Concat\Http\Middleware\Logger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LogLevel;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Gaufrette\Filesystem;
use Tuna\CloudflareMiddleware;

class ExHentaiBrowserService
{
    const BASE_URL         = 'https://exhentai.org/';
    const SAFE_URL         = 'https://e-hentai.org/';
    const API_URL          = 'https://api.e-hentai.org/api.php';
    const LOGIN_URI        = 'https://forums.e-hentai.org/index.php?act=Login&CODE=01';
    const USER_AGENT       =
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36';
    const CACHE_KEY_COOKIE = 'cookie';

    /**
     * @var int
     */
    public $maxDownloadAttempts = 20;

    /**
     * @var array
     */
    private $guzzleContainer = [];

    public $rateLimiterEnabled = true;

    /**
     * @var Middleware
     */
    private $history;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var CookieJar
     */
    private $cookieJar = null;

    /**
     * @var \DateTimeInterface
     */
    private $lastRequest;

    /**
     * @var ResponseInterface
     */
    private $lastResponse;

    /**
     * @var int
     */
    private $requestCounter=0;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ArchiveService
     */
    private $archiveService;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        ArchiveService $archiveService,
        EventDispatcherInterface $eventDispatcher,
        CacheItemPoolInterface $cache,
        ?string $passwordHash = null,
        ?int $memberId = null
    ) {
        $this->archiveService  = $archiveService;
        $this->entityManager   = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger          = $logger;
        $this->cache           = $cache;

        $cacheCookie = $this->cache->getItem(self::CACHE_KEY_COOKIE);

        if (!$cacheCookie->isHit()) {
            if ($memberId && $passwordHash) {
                $cookieParams = [
                    'ipb_member_id' => $memberId,
                    'ipb_pass_hash' => $passwordHash,
                    "hath_perks" => "m1.m2.m3.tf.t1.t2.t3.p1.p2.s-210aa44613",
                ];

                $cookieJar = new CookieJar();

                $domains = ['.ehentai.org', '.exhentai.org', '.e-hentai.org'];
                foreach ($domains as $domain) {
                    foreach ($cookieParams as $key => $value) {
                        $cookieJar->setCookie(new SetCookie([
                            'Name' => $key,
                            'Value' => $value,
                            'Domain' => $domain
                        ]));
                    }
                }

                $cacheCookie->set($cookieJar);
                $this->cache->save($cacheCookie);
                $this->cookieJar = $cookieJar;
            }
        } else {
            $this->logger->debug('Setting cookiejar from login (construct)');
            $this->cookieJar = $cacheCookie->get();
        }

        $this->initClient();
    }

    public function setFilesystem(Filesystem $filesystem): self
    {
        $this->fileSystem = $filesystem;

        return $this;
    }

    private function initClient()
    {
        $this->history = Middleware::history($this->guzzleContainer);
        $stack = HandlerStack::create();
        $stack->push($this->history);
        $stack->push(CloudflareMiddleware::create());

        $middleware = new Logger($this->logger);
        $middleware->setLogLevel(LogLevel::DEBUG);
        $middleware->setRequestLoggingEnabled(true);
        $middleware->setFormatter(new MessageFormatter(MessageFormatter::DEBUG));

        $clientArg = [
            'base_uri' => self::BASE_URL,
            'defaults' => [
                'allow_redirects' => [
                    'max'             => 5,
                    'refer'           => true,
                    'track_redirects' => true
                ],
                'headers' => [
                    'User-Agent' => self::USER_AGENT
                ]
            ],
            'handler' => $stack
        ];

        if (!$this->cookieJar) {
            $this->cookieJar = new CookieJar();
        }
        $clientArg['cookies'] = $this->cookieJar;

        $this->client = new Client($clientArg);
    }

    public function login(string $username, string $password): bool
    {
        $this->logger->debug('Logging in via form');
        $cacheCookie = $this->cache->getItem(self::CACHE_KEY_COOKIE);

        if ($cacheCookie->isHit()) {
            $this->logger->debug('Loading cookiejar from cache (login)');
            $this->cookieJar = $cacheCookie->get();
            return true;
        }

        $response = $this->getClient()->post(
            self::LOGIN_URI,
            [
                'form_params' => [
                    'CookieDate'       => 1,
                    'b'                => 'd',
                    'bt'               => '1-1',
                    'UserName'         => $username,
                    'PassWord'         => $password,
                    'ipb_login_submit' => 'Login!'
                ]
            ]
        );

        if ($response->getStatusCode() == 200) {
            if ($response->getHeader('Set-Cookie')) {
                $this->logger->info('Login success');
                $this->logger->debug('Saving cookiejar in cache');

                $cacheCookie->set($this->cookieJar);
                $this->cache->save($cacheCookie);

                return true;
            }
        }

        return false;
    }

    public function logout()
    {
        $this->cookieJar->clear();
    }

    public function getTagSearchQuery(string $tag)
    {
        if (strpos($tag, '$') === false) {
            $tag = $tag.'$';
        }
        if (strpos($tag, ':') !== false) {
            list($namespace, $tagname) = explode(':', $tag);
            // Tags containing spaces in name are quoted
            if (strpos($tagname, ' ') !== false) {
                $tag = $namespace.':"'.$tagname. '"';
            }
        }

        return str_replace(':', '%3A', $tag);
    }

    public function getByTag(string $tag, int $page = null)
    {
        return $this->searchRemote($this->getTagSearchQuery($tag), $page);
    }

    public function search(string $query, int $page = null)
    {
        return $this->searchRemote($query, $page);
    }

    public function searchRemote(string $query, int $page = null)
    {
        if (strpos($query, ':') !== false) {
            return $this->getByTag($query, $page);
        }

        return $this->getGalleriesFromOverview($this->get('/', [
            'f_search' => $query,
            'page'   => $page
        ]));
    }

    public function searchLocal(string $query, int $page = null)
    {
    }

    /**
     * @param int|null $page
     * @return ArrayCollection
     * @throws HentaiException
     */
    public function getIndex(int $page = null)
    {
        $data = $this->get('/', ['page' => $page]);
        return $this->getGalleriesFromOverview($data);
    }

    private function getGalleriesFromOverview(string $html)
    {
        if (preg_match_all(
            '~https:\/\/e(?:-|x)hentai.org\/g\/([0-9]+)\/([0-9a-f]+)\/~i',
            $html,
            $matches,
            PREG_PATTERN_ORDER
        )) {
            $tokenList = [];

            for ($i=0; $i < count($matches[0]); $i++) {
                $tokenList[$matches[1][$i]] = new GalleryToken($matches[1][$i], $matches[2][$i]);
            }

            return $this->getGalleries($tokenList);
        }

        throw new HentaiException('No galleries found in overview');
    }

    public function getGallery(int $id, string $token): ExhentaiGallery
    {
        $this->logger->debug('getGallery', [
            'id' => $id,
            'token' => $token
        ]);
        return $this->getGalleries([new GalleryToken($id, $token)])[0];
    }

    /**
     * @param GalleryToken[] ...$tokens
     * @return ArrayCollection[ExhentaiGallery]
     */
    public function getGalleries(array $tokens) :ArrayCollection
    {
        $this->logger->debug('getGalleries', [
            'galleries' => array_map(function ($item) {
                return [
                    'id' => $item->getId(),
                    'token' => $item->getToken()
                ];
            }, $tokens)
        ]);

        $galleries = new ArrayCollection();
        // Check if we're trying to lookup more than 25 galleries (API LIMIT)
        // If so, split up and request per 25 galleries
        if (count($tokens) > 25) {
            $this->logger->debug('Requested more than 25 galleries, batch processing request', [
                'amount' => count($tokens)
            ]);
            $apiCalls = ceil(count($tokens)/25);

            for ($i=0; $i<=$apiCalls; $i++) {
                $galleryTokens = array_splice($tokens, 0, 25);
                $newGalleries = $this->getGalleries($galleryTokens);
                if ($newGalleries->count()) {
                    $newGalleries->forAll(function($key, $item) use ($galleries) {
                        $galleries->add($item);
                    });
                }
            }
        } else {
            $gidList = [];
            /** @var GalleryToken $token */
            foreach ($tokens as $token) {
                $gidList[] = [
                    $token->getId(),
                    $token->getToken()
                ];
            }

            $apiGalleries = $this->api('gdata', [
                'gidlist' => $gidList,
                'namespace' => 1
            ]);

            $response = json_decode($apiGalleries->getBody()->getContents());

            if (isset($response->gmetadata)) {
                foreach ($response->gmetadata as $metadata) {
                    $this->logger->debug('Converting API object to Gallery');
                    $gallery = $this->entityManager->getRepository(ExhentaiGallery::class)->fromApi($metadata);

                    $this->eventDispatcher->dispatch(
                        ExhentaiGalleryCreatedEvent::NAME,
                        new ExhentaiGalleryCreatedEvent($gallery)
                    );

                    $galleries->add($gallery);
                }
            }
        }

        return $galleries;
    }

    public function getGalleryPage(string $token, int $galleryId, int $page = null)
    {
    }

    public function downloadGallery(ExhentaiGallery $gallery, $method = 'zip')
    {
        switch ($method) {
            case 'zip':
                return $this->downloadGalleryZip($gallery);
                break;
            case 'scrape':
                return $this->downloadGalleryScrape($gallery);
                break;
            case 'hath':
                return $this->downloadGalleryHath($gallery);
                break;
            default:
                throw new HentaiException('download method not supported');
        }
    }

    public function downloadGalleryZip(ExhentaiGallery $gallery, $resampled = false)
    {
        $this->logger->info('Downloading gallery zip', [
            'gallery_id'     => $gallery->getId(),
            'gallery_title'  => $gallery->getTitle(),
            'resampled'      => $resampled
        ]);
        $downloadSuccess = false;

        if ($gallery->getArchiverKey()->getTime() < new \DateTime('-24 hour')) {
            $this->logger->info('renewing Archiver key');
            // Renew Archiver key
            $currentToken = $gallery->getArchiverKey()->getToken();
            $newToken = $this->getGallery($gallery->getId(), $gallery->getToken())->getArchiverKey();
            $gallery->updateArchiverKey($newToken->getToken(), $newToken->getTime());

            $this->logger->debug('Updated archiver key', [
                'old' => $currentToken,
                'new' => $gallery->getArchiverKey()->getToken()
            ]);
        }

        // Cooldown (test)
        sleep(1);

        $archiverPageHtml = $this->get(self::SAFE_URL.'archiver.php', [
            'gid'   => $gallery->getId(),
            'token' => $gallery->getToken(),
            'or'    => $gallery->getArchiverKey()->getToken()
        ]);

        $formOffset = ($resampled === true) ? 0 : 1;

        $crawler = new Crawler($archiverPageHtml);
        $formNode = $crawler->filterXPath("//div[1]/div/div[{$formOffset}]/form");

        if ($formNode->count()) {
            $this->logger->debug('Download form found. Posting form');
            $downloadQueueHtml = $this->request('POST', $formNode->filter('form')->attr('action'), [
                'form_params' => [
                    'dltype'  => $formNode->filterXPath('//input[@name="dltype"]')->attr('value'),
                    'dlcheck' => $formNode->filterXPath('//div/input[@name="dlcheck"]')->attr('value')
                ]
            ])->getBody()->getContents();

            $crawler = new Crawler($downloadQueueHtml);
            if (strpos($crawler->text(), "Locating archive server") !== false) {
                $this->logger->debug('Locating download server', [
                    'gallery_id' => $gallery->getId()
                ]);
                // Gallery is being archived
                $attempt = 0;
                $ip = null;
                while (!$downloadSuccess) {
                    // This can take a while depending on gallery size and popularity
                    $this->logger->debug('Download attempt', [
                        'gallery_id' => $gallery->getId(),
                        'attempt' => $attempt
                    ]);
                    $url = $crawler->filterXPath('//p[@id="continue"]/a')->attr('href');
                    if ($ip === null) {
                        $ip = parse_url($url, PHP_URL_HOST);
                    }
                    $crawler = new Crawler($this->get($url));

                    $downloadUri = sprintf(
                        "http://%s%s",
                        $ip,
                        $crawler->filterXPath('//a')->attr('href')
                    );

                    if (strpos($downloadUri, 'start=1') !== false) {
                        $serverIp = $this->lastResponse->getheader('Host');
                        $filename = sprintf('%s.zip', $gallery->getId());
                        $guzzlePath = sprintf('data://galleries/%s', $filename);

                        $tmpPath = tempnam(sys_get_temp_dir(), $gallery->getId());

                        $tmpStream = fopen($tmpPath, 'w+');

                        if (false === $tmpStream) {
                            $this->logger->error('Unable to open temp file');
                            throw new HentaiDownloadException($gallery, 'Unable to open temp file', $crawler->html());
                        }

                        $this->logger->info('Starting download', [
                            'host_ip'           => $ip,
                            'download_uri'      => $downloadUri,
                            'download_host'     => $serverIp,
                            'gallery_id'        => $gallery->getId(),
                            'gallery_title'     => $gallery->getTitle(),
                            'target_path'       => $guzzlePath,
                            'tmp_path'          => $tmpPath,
                        ]);

                        $downloadResponse = $this->request('GET', $downloadUri, [ 'sink' => $tmpStream ]);

                        if ($downloadResponse->getStatusCode() === 200) {
                            $this->logger->info('Download succeeded', [
                                'gallery_id'       => $gallery->getId(),
                                'gallery_title'    => $gallery->getTitle(),
                                'target_path'      => $this->archiveService->getArchivePath($gallery),
                                'response_headers' => $downloadResponse->getHeaders(),
                            ]);

                            // Flush tmp file and copy to final destination
                            if (fflush($tmpStream)) {
                                $this->logger->debug('Flushing to temp file', [
                                    'gallery_id'  => $gallery->getId(),
                                    'temp_path'   => $tmpPath
                                ]);
                                rewind($tmpStream);
                            }
                            $this->logger->debug('Creating final file', [
                                'file_path' => $filename,
                            ]);

                            $this->logger->debug('Starting write');
                            $this->fileSystem->write($filename, $tmpStream);
                            $this->logger->debug('Flusing file', [
                                'file' => $filename
                            ]);
                            fclose($tmpStream);

                            $downloadSuccess = true;
                            $gallery->setDownloadState(ExhentaiGallery::DOWNLOAD_STATE_DOWNLOADED);
                        }
                    }
                    // Sleep if we have to rety (it says so in the embedded javascript ;))
                    sleep(1);
                    $attempt++;
                    if ($attempt >= $this->maxDownloadAttempts) {
                        throw new HentaiDownloadException($gallery, 'Unable to download archive as zip. Failed after 20 attempts');
                    }
                }
            }
        } else {
            $this->logger->error('Download form not found', [
                'gallery_id'    => $gallery->getId(),
                'gallery_title' => $gallery->getTitle(),
                'html'          => $archiverPageHtml
            ]);
            // @todo add support for galleries already downloaded
            throw new HentaiDownloadException($gallery, 'Download form not found', $archiverPageHtml);
        }

        return $downloadSuccess;
    }

    public function downloadGalleryScrape(ExhentaiGallery $gallery)
    {
    }

    public function downloadGalleryHath(ExhentaiGallery $gallery)
    {
    }

    public function get(string $uri, array $parameters = [])
    {
        if (count($parameters)) {
            $uri = sprintf('%s?%s', $uri, urldecode(http_build_query($parameters)));
        }

        $this->logger->debug('Sending GET request', [
            'uri'        => $uri,
            'parameters' => $parameters,
            'cookie'     => $this->cookieJar->toArray()
        ]);

        $this->lastResponse = $this->request('GET', $uri);

        $this->logger->debug('RESPONSE', [
            'code' => $this->lastResponse->getStatusCode()
        ]);

        $responseBody = $this->lastResponse->getBody()->getContents();

        return $responseBody;
    }

    private function api(string $method, array $payload): ResponseInterface
    {
        $this->logger->debug('API CALL', [
            'uri' => self::API_URL,
            'payload' => $payload
        ]);
        return $this->request('POST', sprintf(self::API_URL), [
            RequestOptions::JSON => array_merge([
                'method' => $method
            ], $payload)
        ]);
    }

    public function request(string $method, $uri = '/', $parameters = [])
    {
        $this->requestCounter++;
        if (!$this->lastRequest) {
            $this->lastRequest = new \DateTime();
        }

        usleep(mt_rand(200000, 1000000));

        if ($this->requestCounter > 4 && $this->rateLimiterEnabled) {
            sleep(7);
            $this->requestCounter = 0;
        }

        $this->lastResponse = $this->client->request($method, $uri, $parameters);

        return $this->lastResponse;
    }


    public function getHistory()
    {
        return $this->guzzleContainer;
    }

    /**
     * @return Client
     */
    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client): void
    {
        $this->client = $client;
    }

    /**
     * @return CookieJar
     */
    public function getCookieJar(): CookieJar
    {
        return $this->cookieJar;
    }
}
