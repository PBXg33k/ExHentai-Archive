<?php
/**
 * Created by PhpStorm.
 * User: PBX_g33k
 * Date: 11/10/2018
 * Time: 18:25
 */

namespace App\Service;

use App\Entity\ExhentaiGallery;
use App\Exception\HentaiArchiveException;
use App\Exception\HentaiException;
use App\Model\GalleryArchive;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class ArchiveService
{
    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $archiveDir;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        string $archiveDir,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        Filesystem $fileSystem = null
    ) {
        // @todo replace filesystem with Gaufrette
        $this->setArchiveDir($archiveDir);
        $this->logger = $logger;
        $this->fileSystem = ($fileSystem) ?: new Filesystem();
        $this->entityManager = $entityManager;
    }

    /**
     * @return string
     */
    public function getArchiveDir(): string
    {
        return $this->archiveDir;
    }

    /**
     * @param string $archiveDir
     * @throws HentaiException if archive directory does not exist or is not writable
     */
    public function setArchiveDir(string $archiveDir): void
    {
        if (is_dir($archiveDir)) {
            if (is_writable($archiveDir)) {
                $this->archiveDir = $archiveDir;
            } else {
                throw new HentaiException('Archive directory not writable');
            }
        } else {
            throw new HentaiException('Archive directory does not exist or is not a directory');
        }
    }

    public function getArchivePath(ExhentaiGallery $gallery)
    {
        return sprintf('%s%s%s.zip', $this->archiveDir, DIRECTORY_SEPARATOR, $gallery->getId());
    }

    public function exhentaiGalleryExist(ExhentaiGallery $gallery)
    {
        if ($this->fileSystem->exists($this->getArchivePath($gallery))) {
            try {
                return (new GalleryArchive($this->getArchivePath($gallery), $gallery))->checkArchiveValidity();
            } catch (\Exception $exception) {
            }
        }

        return false;
    }

    public function addArchive(GalleryArchive $galleryArchive, $override = false)
    {
        $exist = $this->exhentaiGalleryExist($galleryArchive->getGallery());

        if ((!$exist || $override === true) &&
            $galleryArchive->checkArchiveValidity()
        ) {
            try {
                $this->fileSystem->hardlink(
                    $galleryArchive->getPath(),
                    $this->getArchivePath($galleryArchive->getGallery())
                );
            } catch (IOException $exception) {
                $this->logger->error('Failed hardlinking files', [
                    'source' => $galleryArchive->getPath(),
                    'target' => $this->getArchivePath($galleryArchive->getGallery())
                ]);
                $this->fileSystem->copy(
                    $galleryArchive->getPath(),
                    $this->getArchivePath($galleryArchive->getGallery())
                );
            }

            $this->logger->info('Added archive', [
                'galleryTitle' => $galleryArchive->getGallery()->getTitle(),
                'source'       => $galleryArchive->getPath(),
                'target'       => $this->getArchivePath($galleryArchive->getGallery())
            ]);
            $galleryArchive->getGallery()
                ->setDownloadState(ExhentaiGallery::DOWNLOAD_STATE_DOWNLOADED)
                ->setLastAudit(new \DateTime());

            $this->entityManager->merge($galleryArchive->getGallery());
            $this->entityManager->flush();
            return true;
        }

        throw new HentaiArchiveException(
            $galleryArchive,
            $galleryArchive->getGallery(),
            sprintf('Error adding to gallery %s', $galleryArchive->getGallery()->getTitle())
        );
    }

    public function addArchiveToGallery(GalleryArchive $galleryArchive, ExhentaiGallery $gallery, $override = false)
    {
        return $this->addArchive($galleryArchive->setGallery($gallery), $override);
    }
}
