<?php
namespace App\MessageHandler;

use App\Model\GalleryArchiveQueueItem;
use App\Service\ExHentaiBrowserService;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddArchiveMessageHandler implements MessageHandlerInterface
{
    /**
     * @var ExHentaiBrowserService
     */
    private $browser;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        ExHentaiBrowserService $browserService,
        AdapterInterface $cache,
        LoggerInterface $logger
    ) {
        $this->browser = $browserService;
        $this->cache   = $cache;
        $this->logger  = $logger;
    }

    /**
     * Will add new gallery to the queue for archiving.
     *
     * Item will be picked up by a cronjob which will batch process via API.
     * This in order to prevent hitting the API rate limit and get banned.
     *
     * @param GalleryArchiveQueueItem $galleryArchiveQueueItem
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function __invoke(GalleryArchiveQueueItem $galleryArchiveQueueItem)
    {
        $this->logger->debug('Invoked message handler', [
            'handler'       => __CLASS__,
            'gallery_id'    => $galleryArchiveQueueItem->getId(),
            'gallery_token' => $galleryArchiveQueueItem->getToken(),
        ]);

        $cachedQueue = $this->cache->getItem(GalleryArchiveQueueItem::CACHE_KEY);

        if (!$cachedQueue->isHit()) {
            $this->logger->debug('cache miss');
            $queue = new ArrayCollection();
        } else {
            $this->logger->debug('cache hit');
            $queue = $cachedQueue->get();
        }

        // Check if Gallery has already been queued
        if (!$queue->exists(function ($key, $element) use ($galleryArchiveQueueItem) {
            return ($element->getId() == $galleryArchiveQueueItem->getId())
                && ($element->getToken() == $galleryArchiveQueueItem->getToken());
        })) {
            $this->logger->debug('adding gallery to (cached) queue');
            $queue->add($galleryArchiveQueueItem);

            $cachedQueue->set($queue);
            $this->cache->save($cachedQueue);
        } else {
            $this->logger->warning('Gallery already queued', [
                'gallery_id' => $galleryArchiveQueueItem->getId()
            ]);
        }
    }
}
