<?php
namespace App\MessageHandler;


use App\Entity\ExhentaiGallery;
use App\Event\ExhentaiGalleryCreatedEvent;
use App\Exception\HentaiException;
use App\Message\StartArchiverMessage;
use App\Model\GalleryArchiveQueueItem;
use App\Repository\ExhentaiGalleryRepository;
use App\Service\ExHentaiBrowserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class StartArchiverMessageHandler implements MessageHandlerInterface
{
    const CACHE_KEY = 'ArchiverJob';

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ExhentaiGalleryRepository
     */
    private $galleryRepository;

    /**
     * @var ExHentaiBrowserService
     */
    private $browserService;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(
        ContainerInterface $container,
        LoggerInterface $logger,
        ExHentaiBrowserService $browserService,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->cache = $container->get('cache.app');
        $this->galleryRepository = $container->get('doctrine')
            ->getRepository(ExhentaiGallery::class);
        $this->entityManager     = $container->get('doctrine.orm.entity_manager');
        $this->logger            = $logger;
        $this->browserService    = $browserService;
        $this->eventDispatcher   = $eventDispatcher;
    }

    public function __invoke(StartArchiverMessage $message)
    {
        $this->logger->debug('StartArchiveMessageHandler triggered');
        $this->logger->debug('Registering listener');
        // Register the listener to queue newly added galleries
        $this->eventDispatcher->addListener(
            ExhentaiGalleryCreatedEvent::NAME,
            function (ExhentaiGalleryCreatedEvent $event) {
                $event->getGallery()->setDownloadState(ExhentaiGallery::DOWNLOAD_STATE_QUEUED);
            }
        );

        // Check if we already have a job running.
        $cacheJob = $this->cache->getItem(self::CACHE_KEY);

        if ($cacheJob->isHit() && $cacheJob->get() !== false) {
            throw new HentaiException('Already running ... started on '.$cacheJob->get()->format('c'));
        }

        try {
            $cacheJob->set(new \DateTime());
            $this->cache->save($cacheJob);
            $archiveQueue = $this->cache->getItem(GalleryArchiveQueueItem::CACHE_KEY);

            $this->logger->debug('Loading queued galleries from database');
            $galleries = $this->galleryRepository->getByDownloadState(
                ExhentaiGallery::DOWNLOAD_STATE_QUEUED
            );

            if ($archiveQueue->isHit()) {
                /** @var ArrayCollection[GalleryArchiveQueueItem] $queue */
                $queue = $archiveQueue->get();
                /** @var GalleryArchiveQueueItem $entry */
                $queue = $queue->filter(function ($entry) use ($galleries) {
                    /**
                     * @var ExhentaiGallery $element
                     * @var GalleryArchiveQueueItem $entry
                     */
                    return !$galleries->exists(function ($key, $element) use ($entry) {
                        return $entry->getId() == $element->getId()
                            && $entry->getToken() == $element->getToken();
                    });
                });

                $this->logger->debug('Merging cache & db queue');
                $this->logger->debug('Filtering cache');
                $galleries = new ArrayCollection(array_merge(
                    $galleries->toArray(),
                    $this->browserService->getGalleries($queue->toArray())->toArray()
                ));

                $this->logger->debug('Updating cache queue');
                $archiveQueue->set($queue);
                $this->cache->save($archiveQueue);
            }
            $this->logger->info('Loaded archiver queue. Starting downloads');

            /** @var ExhentaiGallery $gallery */
            foreach ($galleries as $gallery) {
                try {
                    $this->browserService->downloadGalleryZip($gallery);
                    $this->entityManager->merge($gallery);
                    $this->entityManager->flush($gallery);
                } catch (\Exception $exception) {
                    $this->logger->error($exception->getMessage());
                }
            }

            $this->logger->info('Archiver finished run', [
                'gallery_count' => $galleries->count()
            ]);
        } catch (\Exception $exception) {
            $cacheJob->set(false);
            $this->logger->error('Exception occured', [
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage()
            ]);
            throw $exception;
        }

        $cacheJob->set(false);
        $this->cache->save($cacheJob);
    }
}
