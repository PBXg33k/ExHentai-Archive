<?php

namespace App\Controller;

use App\Entity\ExhentaiGallery;
use App\Model\GalleryArchiveQueueItem;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class GalleryController extends Controller
{
    /**
     * @Route("/gallery/index/{page}", defaults={"page": 1}, name="galleryindex")
     */
    public function index($page)
    {
        $finder = $this->container->get('fos_elastica.finder.app.gallery');

        $query = new Query();
        $query->addSort(['posted' => ['order' => 'desc']]);

        $paginator = $finder->findPaginated($query);
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);

        return $this->getJsonResponse($paginator->getCurrentPageResults());
    }

    /**
     * @Route("/gallery/search/{query}/{page}", defaults={"page": 1}, name="search")
     */
    public function search($query, $page)
    {
        $finder = $this->container->get('fos_elastica.finder.app.gallery');

        $boolQuery = new BoolQuery();
        $titleQuery = new Match('title', $query);
        $titleJPQuery = new Match('titleJapan', $query);
        $boolQuery->addShould($titleQuery);
        $boolQuery->addShould($titleJPQuery);

        $paginator = $finder->findPaginated($boolQuery);
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);

        return $this->getJsonResponse($paginator->getCurrentPageResults());
    }

    /**
     * @Route("/gallery/status", methods={"POST"})
     */
    public function galleryStatus(Request $request)
    {
        $gids = $request->get('gids');

        if (!$gids) {
            throw new InvalidParameterException("GalleryID(s) required");
        }

        $statuses = $this
            ->get('doctrine')
            ->getRepository(ExhentaiGallery::class)
            ->getByGalleryIds($gids);

        $response = [];

        if ($statuses) {
            /** @var ExhentaiGallery $status */
            foreach ($statuses as $status) {
                $downloadState = $status->getDownloadState();
                $response[] = [
                    'id'       => $status->getId(),
                    'exists'   => $downloadState == ExhentaiGallery::DOWNLOAD_STATE_DOWNLOADED,
                    'archived' => $downloadState == ExhentaiGallery::DOWNLOAD_STATE_QUEUED
                        || $downloadState == ExhentaiGallery::DOWNLOAD_STATE_DOWNLOADED,
                    'errored'  => $downloadState == ExhentaiGallery::DOWNLOAD_STATE_INVALID,
                ];
            }
        }

        return $this->getJsonResponse($response);
    }

    /**
     * @Route("/gallery/archive/{galleryId}/{token}", name="archive")
     */
    public function archive($galleryId, $token, MessageBusInterface $bus)
    {
        try {
            $bus->dispatch(new GalleryArchiveQueueItem((int)$galleryId, $token));

            return new Response('',Response::HTTP_ACCEPTED);
        } catch (\Exception $exception) {
            return new Response($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a JsonResponse that uses the serializer component if enabled, or json_encode.
     *
     * @param $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    protected function getJsonResponse($data, int $status = 200, array $headers = array(), array $context = array()): JsonResponse
    {
        return new JsonResponse(
            $this->container->get('jms_serializer')->serialize($data, 'json'),
            $status,
            $headers,
            true
        );
    }
}
