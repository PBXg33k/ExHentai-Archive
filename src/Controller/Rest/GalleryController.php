<?php
namespace App\Controller\Rest;

use App\Entity\ExhentaiGallery;
use Elastica\Query;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GalleryController extends FOSRestController
{
    public function getGalleryAction(int $galleryId, Request $request): View
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(ExhentaiGallery::class);

        $gallery = $repo->findOneBy(['id' => $galleryId]);
        if ($gallery) {
            return View::create($gallery, Response::HTTP_OK);
        } else {
            return $this->createNotFoundException();
        }
    }

    /**
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview")
     * @return View
     */
    public function getGalleriesAction(int $page)
    {
        $finder = $this->container->get('fos_elastica.finder.app.gallery');

        $query = new Query();
        $query->addSort(['posted' => ['order' => 'desc']]);

        $paginator = $finder->findPaginated($query);
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);

        return View::create($paginator->getCurrentPageResults());
    }
}
