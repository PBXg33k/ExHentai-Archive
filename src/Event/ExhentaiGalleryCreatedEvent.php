<?php
namespace App\Event;

use App\Entity\ExhentaiGallery;
use Symfony\Component\EventDispatcher\Event;

class ExhentaiGalleryCreatedEvent extends Event
{
    const NAME = 'gallery.created';

    /**
     * @var ExhentaiGallery
     */
    protected $gallery;

    public function __construct(ExhentaiGallery $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return ExhentaiGallery
     */
    public function getGallery(): ExhentaiGallery
    {
        return $this->gallery;
    }
}
