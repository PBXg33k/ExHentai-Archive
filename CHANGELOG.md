# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Added support for Docker
- Elasticsearch
- DailyBonus command
- REST API
- Login via username & password
- Event based triggers
- (internal) Caching
    - Use cache to prevent concurrent jobs
    - Cache cookies
- (internal) Job mechanism for long running jobs and background jobs
- (internal) Composer for external dependencies
### Changed
- Exhentai browser now logs in using username and password. This also loads the cookies as served by server.
- Wrapped legacy code in new Symfony codebase
- Use internal Exhentai client for communication with exhentai servers
- Changed models and database (migrations included)
- Refactored userscript
- File handling now managed using an abstraction layer, support for other storage engines on backlog
- Replaced archive command
- (internal) Replaced libraries used by legacy with the exception of R lib
- (internal) Replaced logging mechanism
### Removed
- Support for Vagrant
- Sphinx

## [1.0.0] - 2017-06-20
[Upstream](https://github.com/Sn0wCrack/ExHentai-Archive)

[Unreleased]: https://github.com/PBXg33k/Exhentai-Archive/compare/e467ef664d942dc787bb4663ecaeecc2938c8db2...HEAD
