// ==UserScript==
// @name       ExHentai Archive
// @match      *://exhentai.org/*
// @match      *://e-hentai.org/*
// @require    https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js
// ==/UserScript==

var baseUrl = 'http://localhost/';
var key = 'changeme';

var archiver = {
    gallerycount: 1,
    start: function() {
        console.log('archiver.start()');
        // Detect view mode
        if($('div.itg').length !== 0) {
            this.gallerycount = $('div.itg').children().length-1;
        }
    },
    queue: [],
    galleryids: [],
    addGalleryToQueue: function(galleryid, callbackFunction) {
        galleryid = parseInt(galleryid);
        this.queue.push({
            galleryid,
            callback: callbackFunction
        });
        this.galleryids.push(galleryid);

        // Queue length equals gallerycount. Time to hit it off
        if(this.queue.length == this.gallerycount) {
            this.fire();
        }
    },
    fire: function() {
        $.ajax({
            url: baseUrl+'gallery/status',
            method: 'POST',
            data: {
                gids: this.galleryids,
                key: key
            },
            context: {
                gids: this.galleryids,
                callbackArray: this.queue
            },
            dataType: 'json'
        }).done(function(data){
            this.existingGalleriesOffset = [];
            var that = this;

            data.forEach(function(item) {
                var arrayOffset = that.gids.indexOf(item.id);
                var callbackItem = that.callbackArray[arrayOffset];

                item.exists = true;

                callbackItem.callback(item);

                // Add offset to list of items to be deleted
                that.existingGalleriesOffset.push(arrayOffset);
            });

            that.existingGalleriesOffset.forEach(function(offset){
                that.callbackArray.splice(offset, 1);
            });

            that.callbackArray.forEach(function(item) {
                var payload = {exists: false};
                item.callback(payload);
            });
        });

        return;
    }
}

archiver.start();


function createArchiveLink(gid, token) {
    var link = $('<a href="#">DL</a>');
    link.data('gid', gid);
    link.data('token', token);

    link.on('click', function() {
        $.ajax({
            url:baseUrl + 'gallery/archive/'+gid+'/'+token,
            method: 'GET',
        }).done(function(data) {
            $(link).css({
                color: '#777',
                pointerEvents: 'none'
            });
        });

        return false;
    });

    return link;
}

$('div#gd5').each(function() { //archive button on gallery detail
    var container = $(this);

    archiver.addGalleryToQueue(gid, function(data) {
        if(data.exists) {
            var p = $('<p class="g2"><img src="//exhentai.org/img/mr.gif"> </p>');
            var link = $('<a href="#" target="_blank">Archived</a>');

            if(data.archived) {
                link.prop('href', baseUrl + '?' + $.param({ action: 'gallery', id: data.id }));
            }
            else if (!data.archived) {
                link.on('click', function() {
                    alert('Not yet downloaded');
                    return false;
                });
            }

            link.appendTo(p);
            $('.g2', container).last().after(p);
        }
        else {
            var p = $('<p class="g2"><img src="//exhentai.org/img/mr.gif"> </p>');
            var link = createArchiveLink(gid, token);
            link.appendTo(p);

            $('.g2', container).last().after(p);
        }
    });
});

$('div.itg').each(function() { //gallery search
    var container = $(this);
    var galleries = $('div.id1', container);
    var gids = [ ];

    galleries.each(function() {
        var galleryContainer = $(this);
        var link = $('div.id2 a', galleryContainer).prop('href');

        var bits = link.split("/");

        var gid = bits[4];
        var token = bits[5];

        archiver.addGalleryToQueue(gid, function(data){
            if (!data.exists) {
                var link = createArchiveLink(gid, token);
                link.css({ fontSize: '9px' });
                link.on('click', function() {
                    $(this).parents('.id1').css({ background: 'green' });
                });

                link.prependTo($('.id44 div', galleryContainer));
            } else {
                var res = "";
                if (data.archived == 1) {
                    res = $('<p>Archived</p>');
                    galleryContainer.css({background: 'green'});
                } else if (data.archived == 0) {
                    galleryContainer.css({background: '#ffaa00'})
                }

                if(res !== "")
                    res.prependTo($('.id44 div', galleryContainer));
            }
        });

        galleryContainer.data('gid', gid);
        $('.id44 img', galleryContainer).remove();

        return;
    });
});
